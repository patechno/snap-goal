import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  lang;

  langList = [
    { code: 'EN', name: 'English Language', url: '/assets/img/eng.jpg' },
    { code: 'AB', name: 'اللغة العربية', url: '/assets/img/palestine.png' }
  ];

  sideNavWidth = 0;

  constructor() { }

  ngOnInit() {
    this.lang = this.langList[0];
  }

  toggleNav() {
    this.sideNavWidth = this.sideNavWidth === 0 ? 250 : 0;
  }
}

import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-filter-header',
  templateUrl: './filter-header.component.html',
  styleUrls: ['./filter-header.component.css']
})
export class FilterHeaderComponent implements OnInit {

  teamList = [
    { id: 1, name: 'Black Eagles', url: '/assets/img/team-1.jpg' },
    { id: 2, name: 'Banana Slugs', url: '/assets/img/team-2.png' },
    { id: 3, name: 'Preachers', url: '/assets/img/team-3.png' },
    { id: 4, name: 'Fighting Cardinals', url: '/assets/img/team-4.png' },
    { id: 5, name: 'The Predators', url: '/assets/img/team-5.png' },
    { id: 6, name: 'Razorbacks', url: '/assets/img/team-6.jpg' },
    { id: 7, name: 'Rebels', url: '/assets/img/team-11.jpg' },
    { id: 8, name: 'Fighting Crusaders', url: '/assets/img/team-8.png' },
    { id: 9, name: 'Avengers', url: '/assets/img/team-9.png' },
    { id: 10, name: 'Aztecs', url: '/assets/img/team-10.png' },
  ];

  @Input() selectedId = -1;
  filter: string;

  @Output() teamIdChange = new EventEmitter<number>();

  constructor(private router: Router) { }

  ngOnInit() {
    this.dropdownClear();
  }

  onTeamSelected(teamId: number) {
    console.log(teamId);
    this.teamIdChange.emit(teamId);
    this.router.navigate(['gallery', teamId]);
  }

  dropdownClear() {
    window.onclick = (event: any) => {
      if (!event.target.matches('.dropbtn')) {
        const dropdowns = document.getElementsByClassName("dropdown-content");
        for (let i = 0; i < dropdowns.length; i++) {
          const openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
  }

}

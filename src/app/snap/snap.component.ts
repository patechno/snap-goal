import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Service } from '../service/service.service';

@Component({
  selector: 'app-snap',
  templateUrl: './snap.component.html',
  styleUrls: ['./snap.component.css']
})
export class SnapComponent implements OnInit {

  id: number;
  sub: Subscription;
  data;
  progress = 0;
  shareLiks = {
    facebook: 'https://www.facebook.com/share.php?u=',
    twitter: 'https://twitter.com/intent/tweet?url='
  };

  constructor(private route: ActivatedRoute, private router: Router, private service: Service) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params.id;
      console.log(this.id);
      this.service.getSpanData(this.id).subscribe((data: any[]) => {
        console.log(data);
        this.data = data;
      });
    });
  }

  previous() {
    this.router.navigate(['snap', this.data[0].id]);
  }

  next() {
    this.router.navigate(['snap', this.data[2].id]);
  }

  timeupdate(player) {
    this.progress = Math.floor((100 / player.duration) * player.currentTime);
  }

  share(social) {
    let win;
    const width = 575;
    const height = 400;
    const url = this.shareLiks[social] + window.location.href;
    const left = (document.documentElement.clientWidth / 2 - width / 2);
    const top = (document.documentElement.clientHeight - height) / 2;
    const opts = 'status=1,resizable=yes' +
      ',width=' + width + ',height=' + height +
      ',top=' + top + ',left=' + left;
    win = window.open(url, '', opts);
    win.focus();
  }
}

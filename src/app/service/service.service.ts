import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Service {

  data;

  constructor(private http: HttpClient) { }

  getGalleryData() {
    return this.http.get('/assets/data/gallery-data.json').pipe(map((data: any[]) => {
      this.data = data;
      return data;
    }));
  }

  getGalleryDataByTeamId(teamId: number) {
    return this.http.get('/assets/data/gallery-data.json').pipe(map((data: any) => {
      this.shuffle(data);
      this.data = data;
      return data;
    }));
  }

  getSpanData(id: number) {
    if (this.data) {
      return of(this.createSpanData(this.data, id));
    }
    return this.getGalleryData().pipe(map(data => {
      return this.createSpanData(data, id);
    }));
  }

  createSpanData(data, id) {
    const index = data.findIndex(x => x.id === +id);
    return [
      data[index === 0 ? data.length - 1 : index - 1],
      data[index],
      data[(index + 1) % (data.length - 1)]
    ]
  }

  shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }
}

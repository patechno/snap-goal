import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'divide'
})
export class DividePipe implements PipeTransform {

  transform(value: any[], n?: number, size?: number): any {
    if (value && n > -1 && size > 0) {
      let position = 0;
      return value.filter((x, i) => {
        if (i === n + position) {
          position += size;
          return true;
        }
        return false;
      });
    }
    return null;
  }

}

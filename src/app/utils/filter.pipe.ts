import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(array: any[], filter?: string): any {
    if (filter) {
      return array.filter(x => x.name.toUpperCase().indexOf(filter.toUpperCase()) > -1);
    }
    return array;
  }

}

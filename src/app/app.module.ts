import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GalleryComponent } from './gallery/gallery.component';
import { SnapComponent } from './snap/snap.component';
import { FilterPipe } from './utils/filter.pipe';
import { DividePipe } from './utils/divide.pipe';
import { HeaderComponent } from './layout/header/header.component';
import { FilterHeaderComponent } from './layout/filter-header/filter-header.component';

@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    SnapComponent,
    FilterPipe,
    DividePipe,
    HeaderComponent,
    FilterHeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

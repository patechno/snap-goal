import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { trigger, style, state, transition, animate } from '@angular/animations';

import { Subscription } from 'rxjs';
import { Service } from '../service/service.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css'],
  animations: [trigger('fadeInOut', [
    state('void', style({
      opacity: 0
    })),
    transition('void <=> *', animate(1000)),
  ])]
})
export class GalleryComponent implements OnInit {

  teamId: number;
  sub: Subscription;
  data = [];
  loaded = true;

  constructor(private route: ActivatedRoute, private rotuer: Router, private service: Service) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.teamId = params.teamId;
      this.loadMore(false);
    });
    window.onscroll = () => {
      if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight && this.loaded) {
        console.log('load more data!');
        this.loadMore(true);
        this.loaded = false;
        setTimeout(() => this.loaded = true, 500);
      }
    };
  }

  loadMore(loadMore) {
    if (this.teamId) {
      this.service.getGalleryDataByTeamId(this.teamId).subscribe((data: any[]) => {
        this.data = loadMore ? [...this.data, ...data] : [...data];
      });
    } else {
      this.service.getGalleryData().subscribe((data: any[]) => {
        this.data = loadMore ? [...this.data, ...data] : [...data];
      });
    }
  }

  onTeamSelected(teamId) {
    console.log(teamId);
    this.rotuer.navigate(['gallery', teamId]);
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GalleryComponent } from './gallery/gallery.component';
import { SnapComponent } from './snap/snap.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'gallery' },
  {
    path: 'gallery', children: [
      { path: '', component: GalleryComponent },
      { path: ':teamId', component: GalleryComponent }
    ]
  },
  { path: 'snap/:id', component: SnapComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
